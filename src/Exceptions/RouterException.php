<?php
/**
 * Created by PhpStorm.
 * User: Xavier Yang
 * Date: 2018/12/31
 * Time: 18:16
 * Email:499873958@qq.com
 */

namespace Xavier\Exceptions;


use Throwable;

class RouterException extends BaseException
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}