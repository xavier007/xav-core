<?php
/**
 * Created by PhpStorm.
 * User: Xavier Yang
 * Date: 2018/12/28
 * Time: 11:07
 * Email:499873958@qq.com
 */
declare(strict_types=1);

namespace Xavier\Exceptions;


class TcpException extends BaseException
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}