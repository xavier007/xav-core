<?php
/**
 * Created by PhpStorm.
 * User: Xavier Yang
 * Date: 2018/12/25
 * Time: 11:39
 * Email:499873958@qq.com
 */
declare(strict_types=1);

namespace Xavier;

use \Xavier\Facade\Container;

class SwooleServer
{
    /**
     * 配置信息
     * @var array
     */
    protected $conf = [];

    /**
     * Server
     * @var null
     */
    protected $server = null;

    /**
     * 协议
     * @var ProtocolAbstract
     */
    protected $protocol = null;

    public function __construct(\Swoole\Server $server, array $conf)
    {
        $this->server = $server;
        $this->conf   = $conf;
        if (isset($conf['pack_protocol'])) {
            $this->protocol = $conf['pack_protocol'];
        }
    }

    public function onStart(\Swoole\Server $server)
    {
        Container::set("server", $server);
    }

    public function onShutdown(\Swoole\Server $server)
    {

    }

    public function onWorkerStart(\Swoole\Server $server, $worker_id)
    {

    }

    public function onWorkerStop(\Swoole\Server $server, $worker_id)
    {

    }

    public function onWorkerExit(\Swoole\Server $server, $worker_id)
    {

    }

    public function onWorkerError(\Swoole\Server $server, $worker_id, $worker_pid, $exit_code, $signal)
    {

    }

    public function onClose(\Swoole\Server $server, $fd, $reactor_id)
    {

    }

    public function onPipeMessage(\Swoole\Server $server, $src_worker_id, $message)
    {

    }

    public function onManagerStart(\Swoole\Server $server)
    {

    }

    public function onManagerStop(\Swoole\Server $server)
    {

    }

    public function __call($name, $arguments)
    {
        if (method_exists($this->server, $name)) {
            return $this->server->$name(...$arguments);
        } else {
            throw new \Exception('方法不存在:' . $name);
        }

    }
}