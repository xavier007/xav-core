<?php
/**
 * Created by PhpStorm.
 * User: Xavier Yang
 * Date: 2018/12/30
 * Time: 16:58
 * Email:499873958@qq.com
 */

namespace Xavier;


use Xavier\Exceptions\RouterException;

class Router
{
    public function init(Request $request, Response $response)
    {

    }

    public function initRouters()
    {
        $routeCollector = Container::get(\FastRoute\RouteCollector::class);
        $dispatcher     = new \FastRoute\Dispatcher\GroupCountBased($routeCollector->getData());
        Container::set(\FastRoute\Dispatcher\GroupCountBased::class, $dispatcher);
    }

    public function __call($name, $arguments)
    {
        // TODO: Implement __call() method.
        $routeCollector = Container::get(\FastRoute\RouteCollector::class);
        if (method_exists($routeCollector, $name)) {
            return $routeCollector->{$name}(...$arguments);
        }
        throw new RouterException("Router不存在该方法");
    }
}