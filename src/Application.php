<?php
/**
 * Created by PhpStorm.
 * User: Xavier Yang
 * Date: 2018/12/27
 * Time: 17:48
 * Email:499873958@qq.com
 */
declare(strict_types=1);

namespace Xavier;


class Application
{
    const SWOOLE_SERVER           = 0;
    const SWOOLE_HTTP_SERVER      = 1;
    const SWOOLE_WEBSOCKET_SERVER = 2;

    private $path;
    private $conf=[];
    private $_server;

    public function setPath(string $path)
    {
        $this->path = $path;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function init()
    {
        \Xavier\Facade\Container::set(ObjectPool::class);
        \Xavier\Facade\Container::set(ConnectPools::class);
        \Xavier\Facade\Container::set(Router::class);

        $options        = [
            'routeParser'    => 'FastRoute\\RouteParser\\Std',
            'dataGenerator'  => 'FastRoute\\DataGenerator\\GroupCountBased',
            'dispatcher'     => 'FastRoute\\Dispatcher\\GroupCountBased',
            'routeCollector' => 'FastRoute\\RouteCollector',
        ];
        $routeCollector = new $options['routeCollector'](
            new $options['routeParser'], new $options['dataGenerator']
        );

        \Xavier\Facade\Container::set(\FastRoute\RouteCollector::class, $routeCollector);

        $this->conf=\Xavier\Facade\Config::get();
    }

    public function initConfig($apppath)
    {
        \Xavier\Facade\Config::initConfig($apppath);
    }

    /**
     * 返回全局server
     * @return Server
     */
    public function getServer()
    {
        return $this->_server;
    }


    private static function e($str)
    {
        echo $str . "\n";
    }

    private function getPid($k)
    {
        $name = 'one_master_' . md5(serialize($this->conf));
        $str  = exec("ps -ef | grep {$name} | grep -v grep");
        $arr  = explode(' ', $str);
        $arr  = array_filter($arr, 'trim');
        $arr  = array_values($arr);
        if (empty($arr) && ($k === 'reload' || $k === 'stop')) {
            exit("程序未运行\n");
        }
        return $arr[1];
    }

    private function shell()
    {
        global $argv;
        $k    = trim(end($argv));
        $name = 'one_master_' . md5(serialize($this->conf));
        if ($k === 'reload') {
            $id = $this->getPid('reload');
            exec("kill -USR1 {$id}");
            exit("reload succ\n");
        } else if ($k === 'stop') {
            $id = $this->getPid('stop');
            exec("kill {$id}");
            exit("stop succ\n");
        }

    }

    public function runAll()
    {
        $this->shell();
        if ($this->_server === null) {
            $this->_check();
            list($swoole, $server) = $this->startServer($this->conf['server']);
            $this->addServer($swoole, $server);
            $this->_server = $server;
            $this->e('server start');
            @swoole_set_process_name('xavier_master_' . md5(serialize($this->conf)));
            $server->start();
        }
    }

    private function startServer($conf)
    {
        $server = null;
        switch ($conf['server_type']) {
            case self::SWOOLE_WEBSOCKET_SERVER:
                $server = new \swoole_websocket_server($conf['ip'], $conf['port'], $conf['mode'], $conf['sock_type']);
                break;
            case self::SWOOLE_HTTP_SERVER:
                $server = new \swoole_http_server($conf['ip'], $conf['port'], $conf['mode'], $conf['sock_type']);
                break;
            case self::SWOOLE_SERVER:
                $server = new \swoole_server($conf['ip'], $conf['port'], $conf['mode'], $conf['sock_type']);
                break;
            default:
                echo "未知的服务类型\n";
                exit;
        }
        $_server_name = [
            self::SWOOLE_WEBSOCKET_SERVER => 'swoole_websocket_server',
            self::SWOOLE_HTTP_SERVER      => 'swoole_http_server',
            self::SWOOLE_SERVER           => 'swoole_server',
        ];

        $this->e("server {$_server_name[$conf['server_type']]} {$conf['ip']} {$conf['port']}");

        if (isset($conf['set'])) {
            $server->set($conf['set']);
        }

        $e = ['workerstart' => 'onWorkerStart', 'managerstart' => 'onManagerStart'];

        $obj = $this->onEvent($server, $conf['action'], $server, $conf, $e);

        return [$server, $obj];
    }

    private function addServer(\swoole_server $swoole, $server)
    {
        if (!isset($this->conf['add_listener'])) {
            return false;
        }
        foreach ($this->conf['add_listener'] as $conf) {
            $port = $swoole->addListener($conf['ip'], $conf['port'], $conf['type']);
            $this->e("addListener {$conf['ip']} {$conf['port']}");
            if (isset($conf['set'])) {
                $port->set($conf['set']);
            }
            $this->onEvent($port, $conf['action'], $server, $conf);
        }
    }

    private function onEvent($sev, $class, $server, $conf, $call = [])
    {
        $rf    = new \ReflectionClass($class);
        $funcs = $rf->getMethods(\ReflectionMethod::IS_PUBLIC);
        $obj   = new $class($server, $conf);

        foreach ($funcs as $func) {
            if (strpos($func->class, 'Xavier\\Swoole\\') === false) {
                if (substr($func->name, 0, 2) == 'on') {
                    $call[strtolower(substr($func->name, 2))] = $func->name;
                }
            }
        }

        if (isset($call['receive'])) {
            $call['receive'] = '__receive';
        }

        foreach ($call as $e => $f) {
            $sev->on($e, [$obj, $f]);
        }

        return $obj;

    }


    private function _check()
    {
        if (count($this->conf) == 0) {
            echo "请配置服务信息\n";
            exit;
        }

        if (!isset($this->conf['server'])) {
            echo "请配置主服务信息\n";
            exit;
        }

        if (isset($this->conf['add_listener'])) {
            $arr = $this->conf['add_listener'];
        } else {
            $arr = [];
        }

        $arr[] = $this->conf['server'];

        $l   = count($arr);
        $arr = set_arr_key($arr, 'port');

        if (count($arr) != $l) {
            echo "配置服务信息错误: 端口重复\n";
            exit;
        }

        foreach ($arr as $c) {
            if (!isset($c['action'])) {
                echo "配置服务信息错误: 缺少action\n";
                exit;
            }
        }
    }
}