<?php
/**
 * Created by PhpStorm.
 * User: Xavier Yang
 * Date: 2018/12/31
 * Time: 18:17
 * Email:499873958@qq.com
 */

namespace Xavier\Facade;


use Xavier\Facade;

class Router extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Container::get(\Xavier\Router::class);
    }
}