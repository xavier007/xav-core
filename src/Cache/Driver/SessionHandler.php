<?php
/**
 * Created by PhpStorm.
 * User: Xavier Yang
 * Date: 2018/12/29
 * Time: 16:23
 * Email:499873958@qq.com
 */

namespace Xavier\Cache;


class SessionHandler implements \SessionHandlerInterface
{
    private $prefix = 'xav_sn_' ;

    private $expire_time = 1200;

    /**
     * Session constructor.
     * @param int $expire_time 过期时间
     */
    public function __construct($expire_time)
    {
        $this->expire_time = $expire_time;
    }

    public function close()
    {
        return true;
    }

    public function destroy($session_id)
    {
        return Cache::del($this->prefix .$session_id);
    }

    public function gc($maxlifetime)
    {
        return true;
    }

    public function open($save_path, $name)
    {
        return true;
    }

    public function read($session_id)
    {
        return Cache::get($this->prefix.$session_id);
    }

    public function write($session_id, $session_data)
    {
        return Cache::setex($this->prefix.$session_id,$this->expire_time,$session_data);

    }
}