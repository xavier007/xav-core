<?php
/**
 * Created by PhpStorm.
 * User: Xavier Yang
 * Date: 2018/12/31
 * Time: 18:38
 * Email:499873958@qq.com
 */

namespace Xavier\Swoole;


use Xavier\Facade\Container;
use Xavier\Session;

class Response
{

    /**
     * @var Request
     */
    protected $httpRequest;
    private $httpResponse;

    protected $_session = null;


    public function init(Request $request, \Swoole\Http\Response $response)
    {
        $this->httpRequest  = $request;
        $this->httpResponse = $response;
    }

    public function header($key, $val, $replace = true, $code = null)
    {
        $this->httpResponse->header($key, $val);
        if ($code) {
            $this->code($code);
        }
    }

    public function code($code)
    {
        $this->httpResponse->status($code);
    }

    public function getHttpRequest()
    {
        return $this->httpRequest;
    }

    /**
     * @return Session
     */
    public function session()
    {
        if (!$this->_session) {
            $this->_session = Container::make(Session::class, $this);
        }
        return $this->_session;
    }

    public function cookie(...$args)
    {
        $this->httpResponse->cookie(...$args);
    }

    public function write($html)
    {
        $this->httpResponse->write($html);
    }

    /**
     * @param string $data
     * @param null|string $callback
     * @return string
     */
    public function json($data, $callback = null)
    {
        $this->header('Content-type', 'application/json');
        if ($callback) {
            return $callback . '(' . $data . ')';
        } else {
            return $data;
        }
    }


    private $status_texts = [
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',            // RFC2518
        103 => 'Early Hints',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',          // RFC4918
        208 => 'Already Reported',      // RFC5842
        226 => 'IM Used',               // RFC3229
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',    // RFC7238
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Payload Too Large',
        414 => 'URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',                                               // RFC2324
        421 => 'Misdirected Request',                                         // RFC7540
        422 => 'Unprocessable Entity',                                        // RFC4918
        423 => 'Locked',                                                      // RFC4918
        424 => 'Failed Dependency',                                           // RFC4918
        425 => 'Reserved for WebDAV advanced collections expired proposal',   // RFC2817
        426 => 'Upgrade Required',                                            // RFC2817
        428 => 'Precondition Required',                                       // RFC6585
        429 => 'Too Many Requests',                                           // RFC6585
        431 => 'Request Header Fields Too Large',                             // RFC6585
        451 => 'Unavailable For Legal Reasons',                               // RFC7725
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',                                     // RFC2295
        507 => 'Insufficient Storage',                                        // RFC4918
        508 => 'Loop Detected',                                               // RFC5842
        510 => 'Not Extended',                                                // RFC2774
        511 => 'Network Authentication Required',                             // RFC6585
    ];

    /**
     * 页面跳转
     * @param $url
     * @param array $args
     */
    public function redirect($url, $args = [])
    {
        if (isset($args['time'])) {
            $this->header('Refresh', $args['time'] . ';url=' . $url);
        } else if (isset($args['httpCode'])) {
            $this->header('Location', $url, true, $args['httpCode']);
        } else {
            $this->header('Location', $url, true, 302);
        }
        return '';
    }

    public function end(string $html)
    {
        $this->httpResponse->end($html);
        Container::release($this->_session);
    }
}