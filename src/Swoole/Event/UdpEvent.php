<?php
/**
 * Created by PhpStorm.
 * User: Xavier Yang
 * Date: 2018/12/25
 * Time: 14:40
 * Email:499873958@qq.com
 */
declare(strict_types=1);

namespace Xavier\Swoole\Event;


trait UdpEvent
{
    public function onPacket(\Swoole\Server $server, $data, array $client_info)
    {

    }

    public function onReceive(\Swoole\Server $server, $fd, $reactor_id, $data)
    {

    }
}