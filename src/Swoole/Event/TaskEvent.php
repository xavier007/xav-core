<?php
/**
 * Created by PhpStorm.
 * User: Xavier Yang
 * Date: 2018/12/25
 * Time: 14:39
 * Email:499873958@qq.com
 */
declare(strict_types=1);

namespace Xavier\Swoole\Event;


trait TaskEvent
{
    public function onTask(\Swoole\Server $server, $task_id, $src_worker_id, $data)
    {

    }

    public function onFinish(\Swoole\Server $server, $task_id, $data)
    {

    }
}