<?php
/**
 * Created by PhpStorm.
 * User: Xavier Yang
 * Date: 2018/12/25
 * Time: 14:39
 * Email:499873958@qq.com
 */
declare(strict_types=1);

namespace Xavier\Swoole\Event;


trait TcpEvent
{
    public function onConnect(\Swoole\Server $server, $fd, $reactor_id)
    {

    }

    public function onReceive(\Swoole\Server $server, $fd, $reactor_id, $data)
    {


    }

    public function onBufferFull(\Swoole\Server $server, $fd)
    {


    }

    public function onBufferEmpty(\Swoole\Server $server, $fd)
    {


    }
}