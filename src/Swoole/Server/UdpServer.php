<?php
/**
 * Created by PhpStorm.
 * User: Xavier Yang
 * Date: 2018/12/25
 * Time: 14:18
 * Email:499873958@qq.com
 */
declare(strict_types=1);

namespace Xavier\Swoole\Server;


use Xavier\Swoole\Event\UdpEvent;
use Xavier\SwooleServer;

class UdpServer extends SwooleServer
{
    use UdpEvent;
}