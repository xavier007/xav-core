<?php
/**
 * Created by PhpStorm.
 * User: Xavier Yang
 * Date: 2018/12/25
 * Time: 14:17
 * Email:499873958@qq.com
 */
declare(strict_types=1);

namespace Xavier\Swoole\Server;


use Xavier\Swoole\Event\HttpEvent;
use Xavier\SwooleServer;

class HttpServer extends SwooleServer
{
    use HttpEvent;
}