<?php
/**
 * Created by PhpStorm.
 * User: Xavier Yang
 * Date: 2018/12/28
 * Time: 14:03
 * Email:499873958@qq.com
 */
declare(strict_types=1);

namespace Xavier;

use \Xavier\Facade\Container;

class Server
{
    public function init()
    {
        Container::set(ObjectPool::class);
        Container::set(Config::class);
        Container::set(ConnectPools::class);
        Container::set(Application::class);

        $options        = [
            'routeParser'    => 'FastRoute\\RouteParser\\Std',
            'dataGenerator'  => 'FastRoute\\DataGenerator\\GroupCountBased',
            'dispatcher'     => 'FastRoute\\Dispatcher\\GroupCountBased',
            'routeCollector' => 'FastRoute\\RouteCollector',
        ];
        $routeCollector = new $options['routeCollector'](
            new $options['routeParser'], new $options['dataGenerator']
        );

        Container::set(\FastRoute\RouteCollector::class, $routeCollector);
    }

    public function initConfig()
    {

    }

    public function run()
    {

    }
}