<?php
/**
 * Created by PhpStorm.
 * User: Xavier Yang
 * Date: 2019/1/2
 * Time: 15:00
 * Email:499873958@qq.com
 */

namespace Xavier;



use Xavier\Cache\File;
use Xavier\Cache\Redis;

class Session
{
    private $data = [];

    private $name = '';

    private $session_id = '';

    private $time = 0;

    private $drive;

    private $prefix = 'session_';

    /**
     * Session constructor.
     * @param null $response
     * @param null $id session.id
     */
    public function init($response = null, $id = null)
    {
        $this->name = \Xavier\Facade\Config::get('session.name');

        if ($id) {
            $this->session_id = $id;
        } else if ($response) {
            $this->session_id = $response->getHttpRequest()->cookie($this->name);
            if (!$this->session_id) {
                $this->session_id = sha1(uuid());
            }
        }

        if (!$this->session_id) {
            return;
        }

        $this->time = intval(ini_get('session.gc_maxlifetime'));

        if (\Xavier\Facade\Config::get('session.drive') == 'redis') {
            $this->drive = \Xavier\Facade\Container::get(Redis::class);
        } else {
            $this->drive = \Xavier\Facade\Container::get(File::class);
        }

        if ($response) {
            $response->cookie($this->name, $this->session_id, time() + $this->time, '/');
        }

        $this->data = $this->drive->get($this->prefix . $this->session_id);
    }

    public function getId()
    {
        return $this->session_id;
    }

    public function set($key, $val)
    {
        $this->data[$key] = $val;
    }

    public function get($key = null)
    {
        if ($key) {
            return array_get($this->data, $key);
        } else {
            return $this->data;
        }
    }

    public function del($key)
    {
        unset($this->data[$key]);
    }


    public function __destruct()
    {
        if ($this->session_id) {
            $this->drive->set($this->prefix . $this->session_id, $this->data, $this->time);
        }
    }

}