<?php
/**
 * Created by PhpStorm.
 * User: Xavier Yang
 * Date: 2018/12/28
 * Time: 12:02
 * Email:499873958@qq.com
 */
declare(strict_types=1);

namespace Xavier;


class Controller
{
    //容器
    private $container;
    //请求
    private $request;
    //响应
    private $response;
    //服务器
    private $server;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * 初始化函数
     */
    public function init(Request $request, Response $response, \Swoole\Server $server)
    {
        $this->request  = $request;
        $this->response = $response;
        $this->server   = $server;
        /**
         * 调用初始化方法
         */
        $this->initialize($request, $response, $server);
    }

    protected function initialize(Request $request, Response $response, \Swoole\Server $server)
    {

    }
}